﻿using System;

namespace hello_word
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
             Console.WriteLine("have fun in this world");
              Console.WriteLine("but don't let me see you inside the code");

              
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
